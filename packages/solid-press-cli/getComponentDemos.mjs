import { fromMarkdown } from 'mdast-util-from-markdown'
import { mdxjsEsm } from 'micromark-extension-mdxjs-esm'
import { mdxjsEsmFromMarkdown } from 'mdast-util-mdxjs-esm'
import * as acorn from 'acorn'
import glob from 'fast-glob'



export function getComponentDemos(options = {}) {
    return async (ast, file) => {
        console.log(file);
        if (file.history[0].endsWith('comp.mdx')) {
            let list = await glob(['**.demo.mdx'], {
                cwd: '../cui-solid-docs',
                ignore: [
                    '**/node_modules/**',
                    '**/dist/**',
                ]
            })

            list = list.map(item => {
                return item = '/' + item;
            })
            console.log(list);

            const str = `export const __demos = ${JSON.stringify(list)}`;

            const tree = fromMarkdown(str, {
                extensions: [mdxjsEsm({ acorn, addResult: true })],
                mdastExtensions: [mdxjsEsmFromMarkdown()]
            });
            ast.children.unshift(tree.children[0]);
        }

    }
}