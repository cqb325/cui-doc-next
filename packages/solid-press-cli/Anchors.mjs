import { fromMarkdown } from 'mdast-util-from-markdown'
import { visit } from 'unist-util-visit'
import { mdxjsEsmFromMarkdown } from 'mdast-util-mdxjs-esm'
import { mdxJsx } from 'micromark-extension-mdx-jsx'
import { mdxJsxFromMarkdown } from 'mdast-util-mdx-jsx'
import * as acorn from 'acorn'
import { mdxjsEsm } from 'micromark-extension-mdxjs-esm'
import { toString } from 'hast-util-to-string'
import { headingRank } from 'hast-util-heading-rank'

export function Anchors(options) {
    return async (ast, file) => {
        if (file.history[0].endsWith('comp.mdx')) {
            const anchors = [];
            // 处理AST
            visit(ast, (node, index, parent) => {
                if (node.type === 'heading') {
                    // console.log(node);
                    const label = toString(node).trim();
                    let id = '';
                    if (node.data?.hProperties?.id) {
                        id = node.data.hProperties.id;
                    } else {
                        id = label;
                    }
                    anchors.push({
                        id,
                        depth: node.depth,
                        title: label
                    })
                }
                if (node.type === 'mdxJsxFlowElement' && node.name === 'Demo') {
                    const props = node.attributes;
                    const anchorProps = {};
                    props.forEach(prop => {
                        if (typeof prop.value === 'string') {
                            anchorProps[prop.name] = prop.value;
                        }
                    })
                    anchors.push(anchorProps)
                }
            });

            const importElement =
                fromMarkdown(`import Anchors from 'solid-press/src/components/Anchors.tsx';`, {
                    extensions: [mdxjsEsm({ acorn, addResult: true })],
                    mdastExtensions: [mdxjsEsmFromMarkdown()]
                });

            const anchroNodes = fromMarkdown(
                `<Anchors data={${JSON.stringify(anchors)}}/>`
                , {
                    extensions: [mdxJsx({ acorn, addResult: true })],
                    mdastExtensions: [mdxJsxFromMarkdown()]
                });

            ast.children.push(importElement.children[0]);
            ast.children.push(anchroNodes.children[0]);
        }
    }
}