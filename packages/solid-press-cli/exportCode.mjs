import fs from 'fs';
// import babel from "@babel/core";

// async function transform (sourceCode: string, lang: string = 'ts') {
//     const code = await codeToHtml(sourceCode, {
//         lang: lang,
//         theme: 'github-light',
//         transformers: [
//             {
//                 pre(node) {
//                     // @ts-ignore
//                     this.addClassToHast(node, 'language-ts cm-demo-sourcecode')
//                 },
//                 code(node) {
//                     this.addClassToHast(node, 'language-ts')
//                 },
//                 line(node, line) {
//                     node.properties['data-line'] = line
//                 },
//             },
//         ]
//     })
//     return encodeURIComponent(code);
// }

export function exportCode(options = {}) {
    return {
        name: 'demo-transform',
        async transform(code, id) {
            if (id && id.endsWith('.demo.tsx')) {
                let sourceCode = fs.readFileSync(id, 'utf8');
                // const matched = sourceCode.match(/---([^/]+)---/);
                // if (matched && matched.length) {
                //     sourceCode = sourceCode.replace(matched[0]+'\r\n', '');
                // }

                // const jsCode = babel.transformSync(sourceCode, {
                //     plugins: [
                //         [
                //             "@babel/plugin-transform-typescript",
                //             {
                //                 isTSX: true,
                //                 allExtensions: true,
                //             },
                //         ],
                //     ],
                // });

                //                 code += `

                // export const __jsCode = \`${jsCode?.code || ''}\`;

                //                 `
                code += `
export const code = \`${sourceCode}\`;
                `

            }
            return code;
        },
    }
}