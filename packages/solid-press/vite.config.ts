import { defineConfig } from 'vite';
import solidPlugin from 'vite-plugin-solid';
import path from 'node:path';
import mdx from '@mdx-js/rollup';
import remarkFrontMatter from 'remark-frontmatter';
import remarkMdxFrontmatter from 'remark-mdx-frontmatter'
import rehypeSlug from 'rehype-slug'
import rehypeAutolinkHeadings from 'rehype-autolink-headings'
import remarkGfm from 'remark-gfm'
import remarkEmoji from 'remark-emoji'
import remarkAttributes from 'remark-attributes'
import remarkDirective from 'remark-directive'
import addClasses from 'rehype-add-classes';
// // import includeDirective from './src/node/plugins/includeDirective'
// import exportCode from './src/node/plugins/exportCode';
import {exportCode, getComponentDemos, DemoDirective, Anchors} from 'solid-press-cli';
// import getComponentDemos from './src/node/plugins/getComponentDemos';
// import rehypeShiki from '@shikijs/rehype'
// import { transformerNotationDiff, transformerNotationHighlight } from '@shikijs/transformers'

export default defineConfig({
    plugins: [exportCode(), mdx({
    // remarkPlugins: [[remarkAttributes, {mdx: true}], remarkDirective, includeDirective, remarkEmoji, remarkFrontMatter, remarkMdxFrontmatter, remarkGfm],
    remarkPlugins: [remarkGfm, [remarkAttributes, {mdx: true}], remarkFrontMatter, remarkEmoji, 
        remarkMdxFrontmatter, remarkDirective, DemoDirective, Anchors],
    rehypePlugins: [[rehypeSlug, {prefix: 'sp-'}], rehypeAutolinkHeadings, [addClasses, {
        table: 'sp-table',
        code: 'sp-inline-code',
    }]],
    jsxImportSource: 'solid-jsx'}), solidPlugin()],
    define: {
      'process.env.BABEL_TYPES_8_BREAKING': 'true',
      'process.env.NODE_DEBUG': 'false',
      'preventAssignment': 'true',
    },
    base: './',
    resolve: {
        dedupe: ['solid-js'],
      conditions: ['development', 'browser'],
      alias: {
        '@': path.resolve('./src'),
        'cui-solid-docs': path.resolve('./cui-solid-docs'),
      }
    },
    build: {
        target: 'esnext',
        outDir: path.resolve(__dirname, './dist'),
    },
    worker: {
        rollupOptions: {
        output: {
            entryFileNames: `assets/[name].js`,
        },
        },
    },
    server: {
      port: 5173,
      host: '0.0.0.0',
    },
});
