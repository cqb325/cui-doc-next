
interface Window {
  compiler: Worker;
  formatter: Worker;
  // @ts-expect-error: 2687
  MonacoEnvironment: {
    getWorker: (_moduleId: unknown, label: string) => Worker;
    onigasm: string;
  };
}