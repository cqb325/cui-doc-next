import { render } from 'solid-js/web';
import { createRouter, createWebHashHistory } from 'solid-vue-router';
import Layout from './Layout';
import 'cui-solid/dist/styles/cui.css'
import './index.less';
import { clearDemos } from './store/demos';

import FormatterWorker from 'solid-repl/repl/formatter?worker';
import { languages } from 'monaco-editor';
import onigasm from 'onigasm/lib/onigasm.wasm?url';

const solidTypes: Record<string, string> = import.meta.glob('../../../node_modules/{solid-js,csstype,cui-solid,cui-virtual-list}/**/*.{d.ts,json}', {
    eager: true,
    query: '?raw',
    import: 'default',
});

for (const path in solidTypes) {
    languages.typescript.typescriptDefaults.addExtraLib(solidTypes[path], `file://${path.replace('../../../', '/')}`);
    languages.typescript.javascriptDefaults.addExtraLib(solidTypes[path], `file://${path}`);
}


import editorWorker from 'monaco-editor/esm/vs/editor/editor.worker?worker';
import tsWorker from 'monaco-editor/esm/vs/language/typescript/ts.worker?worker';
import { loadingBar } from 'cui-solid';

const tsw = new tsWorker();
const ew = new editorWorker();
window.MonacoEnvironment = {
    getWorker (_moduleId: unknown, label: string) {
        switch (label) {
        case 'typescript':
        case 'javascript':
            return tsw;
        default:
            return ew;
        }
    },
    onigasm,
};

window.formatter = new FormatterWorker();

// 获取约定的组件页面
const modules = import.meta.glob('cui-solid-docs/**/*.comp.mdx');
const router = createRouter({
    routes: [
        {
            name: 'home',
            path: '/',
            redirect: '/base/button'
        }
    ],
    history: createWebHashHistory()
})

/**
 * 添加动态路由
 */
async function addRoutes () {
    for (const mPath in modules) {
        const mName = mPath.replace(import.meta.env.VITE_DOC_PATH, '').replace('/index.comp.mdx', '');
        const mod: any = await modules[mPath]();
        router.addRoute({
            name: mName,
            path: mName,
            meta: {
                title: mName
            },
            component: mod.default
        })
    }
}

router.beforeEach(async (to, from) => {
    loadingBar.start();
    // 页面切换前清空锚点
    clearDemos();

    if (to.matched?.length) {
        return true;
    } else {
        if (router.getRoutes().length === 1) {
            // 添加动态路由
            await addRoutes();
            return { path: to.path, replace: true};
        }
    }

    // if (to.matched?.length) {
    //     return true;
    // } else {
    //     const Module = await import(/* @vite-ignore */'/src' + to.path + '.mdx');
    //     const { default: RouteComp } = Module;

    //     router.addRoute({
    //         name: to.path,
    //         path: to.path,
    //         meta: {
    //         },
    //         component: () => RouteComp
    //     })
    //     return { path: to.path };
    // }
})

router.afterEach(async (to, from) => {
    loadingBar.finish();
    setTimeout(() => {
        document.documentElement.scrollTop = 0;
    }, 200);

})

render(() => <Layout/>, document.getElementById('root') as HTMLElement);
