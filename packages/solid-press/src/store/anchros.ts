import { createSignal } from "solid-js";
import { createStore } from "solid-js/store";
const [anchors, setAnchors] = createStore<any[]>([]);
export const [currentAnchor, setCurrentAnchor] = createSignal<string>('');

export const clearAnchors = () => {
    setAnchors([]);
}

export const addAnchor = (anchor: any) => {
    setAnchors((prevAnchors) => [...prevAnchors, anchor]);
}
export const getAnchors = () => anchors;