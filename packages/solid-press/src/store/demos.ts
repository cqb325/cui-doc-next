import { createSignal } from "solid-js";
import { createStore } from "solid-js/store";

const [demos, setDemos] = createSignal<any[]>([]);

export const clearDemos = () => {
    setDemos([]);
}

export const addDemo = (demo: any) => {
    setDemos([...demos(), demo]);
}
export const getDemos = () => demos();