import { Table } from "cui-solid";
import { propsColumns } from "../common/columns";
import { addAnchor } from "../store/anchros";

export default function PropsTable (props: any) {
    const { id, title } = props;
    addAnchor({
        id, title
    })
    return <Table columns={props.columns || propsColumns} data={props.data} border size="small" />
}