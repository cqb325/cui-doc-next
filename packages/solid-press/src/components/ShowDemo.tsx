import { Card, Divider, Paragraph, Text } from 'cui-solid';
import { DemoCode } from './Code';
import { currentAnchor } from '../store/anchros';
import SolidLive, { SolidLivePreview } from "solid-live";

export default function ShowDemo (props: any) {
    return <div id={props.id} classList={{"cm-demo-wrap-active": currentAnchor() === props.id}}>
        <SolidLive formatter={window.formatter} id={props.id} scopes={props.scopes}>
            <Card bordered>
                {/* {props.comp} */}
                <SolidLivePreview/>
                <Divider align="left"><Text type="primary">{props.title}</Text></Divider>
                <Paragraph type="secondary" spacing="extended">
                    {props.desc}
                </Paragraph>
                <DemoCode ts={props.code} title={props.title}/>
            </Card>
        </SolidLive>
    </div>
}