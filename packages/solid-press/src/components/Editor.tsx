import { Input } from "cui-solid";
import { babelTransform } from '../../node/plugins/esbuild-plugin-solid';
import { render } from "solid-js/web";
import { createEffect, createSignal } from "solid-js";

export default function Editor () {
    const [m, setM] = createSignal<any>(null);
    const [code, setCode] = createSignal(`import { Button, Space } from 'cui-solid';
export default function Demo () {
    return <Space dir="v">
        <Space dir="h">
            <Button type='primary'>Primary</Button>
            <Button type='success'>Success</Button>
            <Button type='error'>Error</Button>
            <Button type='warning'>Warning</Button>
        </Space>
        <Space dir="h">
            <Button type='default'>Default</Button>
            <Button type='text'>Text Button</Button>
            <Button type='link'>LINK</Button>
            <Button type='dashed'>Dashed BUTTON</Button>
        </Space>
    </Space>
}`);

    let dispose: any;
    const onChange = async (v: string) => {
        setCode(v)
        // const result = await esbuild.build({
        //     entryPoints: ['<stdin>'],
        //     bundle: true,
        //     format: 'esm',
        //     splitting: true,
        //     outdir: 'dist',
        //     write: false,
        //     // jsxFactory: 'createComponent',
        //     // stdin: {
        //     //     contents: v,
        //     //     loader: 'tsx'
        //     // },
        //     plugins: [
        //         esbuildPluginSolid({
        //             v
        //         })
        //     ],
        // });
        // console.log(result.outputFiles[0].text);
    }

    createEffect(async () => {
        const filename = "demo";
        const ret = babelTransform(filename, code());
        console.log(ret);
        
        const url = URL.createObjectURL(new Blob([ret], { type: 'application/javascript' }));
        const m = await import(/* @vite-ignore */url);
        if (dispose) {
            dispose();
        }
        // setM(m.default);
        
        dispose = render(m.default, document.querySelector('#demo1') as HTMLElement);
    })

    return <div>
        <Input type="textarea" rows={10} style={{width: '500px'}} onChange={onChange} value={code()}/>
        <div id="demo1">
        </div>
    </div>
}