import { Anchor } from "./Anchor";
import { setCurrentAnchor } from "../store/anchros";
import { For } from "solid-js";

export default function Anchors (props: any) {
    const onAnchorChange = (key: string) => {
        setCurrentAnchor(key)
    }

    const data = props.data.filter((item: any) => item.depth !== 1)

    const docH = document.documentElement.clientHeight;
    return <div class="sys-ctx-main-right">
        <div class="sys-anchor" style={{position: 'sticky', top: '64px', "font-size": '12px', padding: '16px', "border-radius": '4px'}}>
            <Anchor mode="history" bounds={200} showInk scrollOffset={(docH - 80) / 2} onChange={onAnchorChange}>
                <For each={data}>
                    {(item: any) => {
                        if (item.children) {
                            return <Anchor.Link href={'#'+item.id} title={item.title} />
                        } else {
                            return <Anchor.Link href={'#'+item.id} title={item.title}>
                                <For each={item.children}>
                                    {(subItem: any) => {
                                        return <Anchor.Link href={'#'+subItem.id} title={subItem.title} />
                                    }}
                                </For>
                            </Anchor.Link>
                        }
                    }}
                </For>
            </Anchor>
        </div>
    </div>
}