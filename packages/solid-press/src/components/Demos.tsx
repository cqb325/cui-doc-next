import { Col, Row, Space } from "cui-solid";
import ShowDemo from "./ShowDemo";
import type { Accessor} from "solid-js";
import { createSignal, For, onCleanup, onMount } from "solid-js";
import { getDemos } from "../store/demos";

export default function Demos (props: any) {
    let wrap: any;
    const initColumns = props.columns ?? 1;
    const [columns, setColumns] = createSignal<Date[]>(initColumns === 1 ? [new Date()] : [new Date(), new Date()]);

    const onWrapEntry = (entry: ResizeObserverEntry) => {
        const { width } = entry.contentRect;
        const all = getDemos().length;
        if (all < 2) {
            if (columns().length !== 1) {
                setColumns([new Date()])
            }
            return;
        }
        if (width < 875 && columns().length === 2) {
            setColumns([new Date()])
        } else if (width >= 875 && columns().length === 1) {
            setColumns([new Date(), new Date()])
        }
    }

    onMount(() => {
        const ro = initColumns !== 1 ? new ResizeObserver((entries: ResizeObserverEntry[]) => {
            entries.forEach((entry: ResizeObserverEntry) => onWrapEntry(entry));
        }) : null;
        ro?.observe(wrap);

        onCleanup(() => {
            ro?.unobserve(wrap);
        })
    })
    return <div ref={wrap}>
        <Row gutter={24}>
            {
                <For each={columns()}>
                    {(item: any, colIndex: Accessor<number>) => {
                        const length = columns().length;
                        const colNum = colIndex();
                        return <Col grid={1/length} style={{position: 'static'}}>
                            <Space dir="v">
                                <For each={getDemos()}>
                                    {
                                        (item, index: Accessor<number>) => {
                                            return index() % length === colNum ? <ShowDemo {...item} /> : null
                                        }
                                    }
                                </For>
                            </Space>
                        </Col>
                    }}
                </For>
            }
        </Row>
    </div>
}