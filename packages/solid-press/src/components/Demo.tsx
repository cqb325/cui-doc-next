import {onMount} from 'solid-js';
import { addAnchor } from '../store/anchros';
import { addDemo } from '../store/demos';

export default function Demo (props: any) {
    const {id, title, children, scopes, code} = props;
    addAnchor({
        id, title
    })
    // onMount(async () => {
    // const m: any = await import(/* @vite-ignore */props.src);
    addDemo({
        id, title, desc: children, src: props.src,
        code: code,
        scopes
    });
    // })

    return null;
}