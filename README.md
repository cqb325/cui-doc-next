<p align="center">
    <a href="https://cqb325.gitee.io/cui-solid-doc">
        <img width="200" src="https://gitee.com/cqb325/cui-solid/raw/master/examples/assets/images/logo.svg">
    </a>
</p>

<h1>
CUI-SOLID
    <h3>An UI component library and front-end solution based on solidjs</h3>
</h1>

[![CUI SolidJS](https://img.shields.io/npm/v/cui-solid.svg?style=flat-square)](https://www.npmjs.org/package/cui-solid)
[![NPM downloads](https://img.shields.io/npm/dm/cui-solid.svg?style=flat-square)](https://npmjs.org/package/cui-solid)
[![NPM downloads](https://img.shields.io/npm/dt/cui-solid.svg?style=flat-square)](https://npmjs.org/package/cui-solid)
![JS gzip size](https://img.badgesize.io/https:/unpkg.com/cui-solid/dist/cui.min.esm.js?label=gzip%20size%3A%20JS&compression=gzip&style=flat-square)
![CSS gzip size](https://img.badgesize.io/https://unpkg.com/cui-solid/dist/styles/cui.css?compression=gzip&label=gzip%20size:%20CSS&style=flat-square)

## Docs

[documents on gitee](https://cqb325.gitee.io/cui-solid-doc 'cui-solid-doc')

[documents on github](https://cqb325.github.io/cui-solid-doc 'cui-solid-doc')

## Repository

[Gitee](https://gitee.com/cqb325/cui-solid 'Gitee')

[Github](https://github.com/cqb325/cui-solid 'Github')

## Other Packages
